---
_clone:
    @clone: ssh os
    @include: ssh|/_clone/
    @include: os|/_clone/
@include: |/_clone/

_info:
    title: Space Agent
    desc: |
        An Agent is run from the Pi from where it opens
        a reverse tunnel from the rendezvous server back to the Pi.
         
        This reverse tunnel makes it so that clients can connect to the
        Pi via the rendezvous server, the traffic will go via this third party.
         
        For the Agent to be able to open a reverse tunnel
        from the remote server and back it will need a ssh key
        which pub key is already uploaded already to the server.
        If the Agents private key is not in standard
        location (~/.ssh) then you can refer to it using
        the -e SSHKEYFILE flag when running /open/.
        Note that when running the Agent as a system service we can
        bundle the key with the Agent script to only have one file to copy to the Pi.
         
        To generate a new key pair do:
            space -m ssh /keygen/ -- mynewkey
        Do not use a passphrase if the Agent should be run
        in background. Upload the public key to the server
        and place it in ~/.ssh/authorized_keys for the user whom the Agent will ssh as.
        You can use Space for this too, to upload the pub key to the server, do:
            space -m ssh /addsshkey/ -e SSHHOST=server -e SSHUSER=me -e targetuser=agentuser -e sshpubkeyfile=mynewkey.pub -s sudo
        You might need sudo privileges to do this for another user than yourself, otherwise remove -s sudo
         
        To install the Agent onto the Pi:
            First dump the Agent as a standalone script:
                space -m gitlab.com/rpi-shell/agent /open/ -e SSHHOST=server \
                    -e SSHFLAGS=-ostricthostkeychecking=no \
                    -e agentid=Green -e bindaddress=0.0.0.0:9333 -e localaddress=127.0.0.1:22 \
                    -e SSHUSER=spaceagent -e SSHKEYFILE=mynewkey -d > spaceagent.sh
                chmod +x spaceagent.sh
             
            The ssh key used could either be bundled in the script or it must
            also be copied onto the card along with the agent script.
            Make sure thay the variable SSHKEYFILE points correctly to the key file.
             
            Bundle key file into script:
                Dump the spaceagent.sh script with -e SSHKEYFILE='$0' to have it
                refer to the script file it self, then bundle key with script as:
                echo "exit" >> spaceagent.sh
                cat keyfile >> spaceagent.sh
                chmod 700 spaceagent.sh  # Must be restrictive since it contains a key file.
             
            Then copy the script onto the Pi's sdcard:
                # List block devices and figure out which one is the sdcard:
                space -m gitlab.com/rpi-shell/manage /sdcard/list_devices/
                 
                space -m gitlab.com/rpi-shell/manage /sdcard/mount/ \
                    -e device=/dev/mmcblk0 -s sudo
                 
                space -m gitlab.com/rpi-shell/manage /sdcard/root/cpbin/ \
                        -e file=spaceagent.sh
                        -s sudo
                 
                If not bundled into the script then copy the ssh key file to
                where SSHKEYFILE is pointing to, make sure the keyfile has 600 as permissions.
             
            Create and enable the script as a systemd service:
                space -m systemd /service/create/ -e description="The Green SpaceAgent" \
                      -e root=/mnt/sdcard/root
                      -e service=SpaceAgentGreen \
                      -e execstart=/bin/spaceagent.sh -s sudo
             
                space -m systemd /service/enable/ -e root=/mnt/sdcard/root -e service=SpaceAgentGreen -s sudo
             
            Now when the Pi is booted the Agent should be running in background.
            You can see its status on the Pi (when it is running) using systemd:
                systemctl status SpaceAgentGreen
             
            Often you would want the Agent to run say five minutes every hour, to give
            you a window to connect but to not drain it's battery by being constantly available.
            Add -e timeout=300 when dumping the Agent script and create a systemd timer which starts the
            systemd service. So create but do not enable the Agent service above, instead:
                space -m systemd /timer/create/ -e description="The Green SpaceAgent timer" \
                        -e root=/mnt/sdcard/root
                        -e timer=SpaceAgentGreen \
                        -e oncalendar=hourly -s sudo
                space -m systemd /timer/enable/ -e timer=SpaceAgentGreen \
                        -e root=/mnt/sdcard/root
                        -s sudo
             
            Unmount sd card
                space -m gitlab.com/rpi-shell/manage /sdcard/umount/ \
                    -e device=/dev/mmcblk0 -s sudo
open:
    _info:
        title: Agent open tunnel to rendezvous server
        desc: |
            An agent will run this to create a tunnel
            to the rendezvous server where it will register
            it self with it's ID and the TCP port it is
            connected through.
            A user could then list all connected and live agents,
            and get information about a particular agent that is connected.
    _env:
        - groupid:
            title: The group that the Agent belongs to.
        - agentid:
            title: Unique agent id
        - subid:
            title: Sub id to separate multiple connections from each other
        - reconnect:
            title: Reopen tunnel on failure
            desc: |
                Reconnect with a three seconds delay.
                If no timeout is given then reconnect will try indefinitely to connect.
                If timeout is given then reconnect will stop after the timeout has passed.
            value: ${reconnect:-1}
            values:
                - 0
                - 1
        - timeout:
            title: Set a timeout in seconds for how long the tunnel is kept open
            desc: |
                After the timeout has passed then the tunnel will be closed.
                Any ongoing connections will however be left open.
                If there is a retry connect phase before a successful connect that
                time spent is considered as being part of the timeout span, so that
                a hour timeout will always be one hour regardless of the Agent managed
                to connect or not.
                A value of '0' or empty means no timeout, keep connected indefinitely.
                To reconnect on hangup set reconnect to 1.
            values:
                - 0
                - 3600
        - title:
            title: An optional title for this agent connection
        - healthcheck:
            title: Issue a ping back to the ssh client on regular intervals
            desc: |
                Using this will help the Agent to exit on the server side on the
                case there is an undetected connection loss, to not yield false
                positives about Agents alive.
            value: ${healthcheck:-1}
            values:
                - 0
                - 1
        - SSHHOST:
            title: Host to connect to
        - SSHFLAGS:
            title: Additional SSH flags
            desc: |
                When running the Agent in background you might want to
                set SSHFLAGS=-oStrictHostKeyChecking=no
                to avoid connection error when the host is yet not
                listed in 'known_hosts'.
        - bindaddress:
            title: Remote address to bind to.
            desc: |
                Usually a TCP address 'host:port',
                where host could be '0.0.0.0' to accept
                foreign connections or '127.0.0.1' to only
                accept local connections originating from the
                server it self.
                Port could be a defined TCP port to listen on
                or set to '0' to have a port randomly assigned.
                Bindaddress could also be a UNIX socket if the
                remote server allows that.
        - localaddress:
            title: Local address to route traffic back to
            desc: |
                Usually is '127.0.0.1:22' to hook up ssh login,
                but could be something like '127.0.0.1:80' to forward
                traffic to a local web server.
        - RUN: RPI_AGENT_OPEN -- "${groupid}" "${agentid}" "${subid}" "${bindaddress}" "${localaddress}" "${reconnect}" "${timeout}" "${title}" "${healthcheck}"
list:
    _info:
        title: List all connected agents for a user
        desc: |
            Connected agents are one a per user basis,
            meaning that an Agent connects as a remote user on the system
            and all agents connecting with the same user will be grouped together.
             
            We list agents by the user we connect as
             
            Only active agents are listed, which are those who have
            a fresh timestamp and therefore an open tunnel.
            A timestamp not older than 30 seconds is consider a live agent.
    _env:
        - SSHHOST:
            title: Host to connect to
        - delta:
            title: Diff in seconds to consider as active agent
            value: ${delta:-30}
        - user:
            title: List agents for a specific user
            desc: |
                Usually we list agents for the user we are connecting as.
                We can state another user instead, this will however
                demand that the remote server is setup so that our connecting
                user have permissions to list files inside other users home directories.
        - RUN: RPI_AGENT_LIST -- "${delta}" "${user}"
get:
    _info:
        title: Get specific agent information for a user
        desc: |
            After listing all active agents or a user,
            we could get information for a specific agent
            to whom we want to connect to.
            The information retrieved will tell us what port or socket
            the agent is listening on.
    _env:
        - SSHHOST:
            title: Host to connect to
        - fullagentid:
            title: The full agent id as returned by /list/
            desc: |
                The format is "groupid.agentid.subid".
                This is how the /list/ node will return the list
                of active agents, so we can just feed such a line into
                this node.
        - user:
            title: Get agent for a specific user
            desc: |
                Usually we get agents for the user we are connecting as.
                We can state another user instead, this will however
                demand that the remote server is setup so that our connecting
                user have permissions to read files inside other users home directories.
        - RUN: RPI_AGENT_GET -- "${fullagentid}" "${user}"
connect:
    _info:
        title: Open client forward tunnel to remote server
        desc: |
            This will create a forward tunnel from local computer to the remote server.
            The server will tunnel the traffic further to an Agent.
             
            If the Agent who created a reverse tunnel from the server to the Pi did bind
            to a local interface, then that port is not accessible from the outside, thus
            we need a forward tunnel into the server, and our client will connect to the
            local port instead of directly to the server.
             
            However if the Agent did bind to a public interface (0.0.0.0) then a forward
            tunnel is probably not necessary.
             
            To get the remote address of the Agent use the /get/ node
            and filter out "BindAddress".
             
    _env:
        - SSHHOST:
            title: Host to connect to
        - SSHFLAGS:
        - remoteaddress:
            title: The remote bind address as returned by /get/
            desc: |
                The address that the agent has bound on the server.
                You can take the value directly from the /get/ returned value "BindAddress".
                If Bindaddress has 0.0.0.0 as interface then it will be translated
                to 127.0.0.1.
                 
                This is the last argument of the positional arguments so that it can be provided
                using 'xargs' on a pipeline together with '/get/', 'grep' and 'awk'.

        - localaddress:
            title: The address to bind tunnel locally to
            desc: |
                This will be a local endpoint to where we can
                connect and the traffic will be routed to the server
                and then to the Agent.
        - RUN: RPI_AGENT_CONNECT -- "${localaddress}" "${remoteaddress}"
...
