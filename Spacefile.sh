#==============
#
# RPI_AGENT_GET
#
# Get the details about a specific agent.
#
# Optionally state another user than the connecting user
# to get agent for.
#
#==============
RPI_AGENT_GET()
{
    SPACE_SIGNATURE="fullagentid [user]"
    SPACE_WRAP="SSH_WRAP"
    SPACE_DEP="PRINT"

    local fullagentid="${1}"
    shift

    local user="${1-}"

    if [ -n "${user}" ]; then
        local dir="/home/$user"
        if [ ! -d "${dir}" ]; then
            PRINT "User dir does not exist: ${dir}." "error"
            return 1
        fi
        cd "${dir}"
    fi

    local file="spaceagent/${fullagentid}.txt"
    if [ ! -f "${file}" ]; then
        PRINT "Agent '${fullagentid}' does not exist." "error"
        return 1
    fi

    cat "${file}"
}


# Disable warning about indirectly checking exit code
# shellcheck disable=2181

#===============
#
# RPI_AGENT_LIST
#
# List all active agents.
#
# Optionally state another user than the connecting user
# to list agents for.
#
#
#===============
RPI_AGENT_LIST()
{
    SPACE_SIGNATURE="delta [user]"
    SPACE_WRAP="SSH_WRAP"
    SPACE_DEP="PRINT"

    local delta="${1}"
    shift

    local user="${1-}"

    if [ -n "${user}" ]; then
        local dir="/home/$user"
        if [ ! -d "${dir}" ]; then
            PRINT "User dir does not exist: ${dir}." "error"
            return 1
        fi
        cd "${dir}"
    fi

    local now=
    now=$(date "+%s")

    mkdir -p "spaceagent"
    cd "spaceagent"
    if [ "$?" -gt 0 ]; then
        PRINT "Permission denied to access files for user ${user}." "error"
        return 1
    fi
    for file in *.txt; do
        local ts=
        ts=$(grep "Date:" "${file}")
        ts="${ts##* }"
        local diff=$((now-ts))
        if [ "${delta}" -ge "${diff}" ]; then
            printf "%s\n" "${file%.txt}"
        fi
    done
}

#===============
#
# RPI_AGENT_OPEN
#
# Open a reverse tunnel from remote server to this device.
#
# Optionally define a TCP port or have one provided randomly.
# Bind address could be a UNIX socket, if the remote server allows that.
#
#===============
RPI_AGENT_OPEN()
{
    SPACE_SIGNATURE="groupid:0 agentid subid:0 bindaddress localaddress reconnect:0 timeout:0 title:0 healthcheck:0"
    SPACE_FN="_RPI_AGENT_OPEN"
    SPACE_WRAP="SSH_WRAP"
    SPACE_OUTER="_RPI_AGENT_OPEN_OUTER"
    SPACE_BUILDENV="SSHHOST SSHFLAGS"
    # shellcheck disable=2034
    SPACE_BUILDDEP="PRINT"
    # shellcheck disable=2034
    SPACE_BUILDARGS="${SPACE_ARGS}"
    # shellcheck disable=2034
    SPACE_REDIR=">/dev/null"  # To mute health check ping

    if [ -z "${SSHHOST}" ]; then
        PRINT "Missing SSHHOST variable." "error"
        return 1
    fi

    local groupid="${1:-0}"
    shift

    local agentid="${1}"
    shift

    local subid="${1:-0}"
    shift

    local bindaddress="${1}"
    shift

    local localaddress="${1}"
    shift

    local reconnect="${1:-1}"
    shift

    local timeout="${1:-0}"
    shift

    local title="${1}"
    shift

    local healthcheck="${1:-1}"
    shift

    local randomport=0
    local port="${bindaddress##*:}"
    if [ "${port}" = "0" ]; then
        bindaddress="${bindaddress%:*}:{TUNNELPORT}"
        # Do request random port.
        randomport=1
    fi

    SPACE_OUTERARGS="'${groupid}' '${agentid}' '${subid}' '${bindaddress}' '${localaddress}' '${reconnect}' '${timeout}' '${randomport}'"
    YIELD "SPACE_OUTERARGS"

    # Pass on args to inner function.
    SPACE_ARGS="'${groupid}' '${agentid}' '${subid}' '${bindaddress}' '${localaddress}' '${reconnect}' '{TIMEOUT}' '${title}' '${healthcheck}'"
    YIELD "SPACE_ARGS"

    SSHFLAGS="${SSHFLAGS:+$SSHFLAGS;}-oExitOnForwardFailure=yes;-R${bindaddress}:${localaddress}"
    YIELD "SSHFLAGS"
}

#======================
#
# _RPI_AGENT_OPEN_OUTER
#
# This outer function is used to randomly try ports
# until one found that is free on the server and it will
# also reconnect with the server if disconnected.
#
#======================
_RPI_AGENT_OPEN_OUTER()
{
    SPACE_SIGNATURE="groupid agentid subid bindaddress localaddress reconnect:0 timeout randomport:0"
    SPACE_DEP="STRING_SUBST PRINT OS_KILL_ALL"

    local groupid="${1:-0}"
    shift

    local agentid="${1}"
    shift

    local subid="${1:-0}"
    shift

    local bindaddress="${1}"
    shift

    local localaddress="${1}"
    shift

    local reconnect="${1}"
    shift

    local timeout="${1}"
    shift

    local randomport="${1}"
    shift

    # Setup TERM trap to kill off all descendants to this script,
    # which is important when running in background and user is
    # killing off the script.
    trap 'trap - INT HUP TERM PIPE; OS_KILL_ALL "$$";' INT HUP TERM PIPE

    local RUN_ORIGINAL="${RUN}"
    local startts=
    startts=$(date +%s)
    while true; do
        RUN="${RUN_ORIGINAL}"
        if [ "${randomport}" = "1" ]; then
            local tunnelport=0
            while [ "${tunnelport}" -lt 1024 ] || [ "${tunnelport}" -gt 64000 ]; do
                tunnelport=$(hexdump -n 2 -e '/2 "%u"' /dev/urandom)
            done
            STRING_SUBST "RUN" "{TUNNELPORT}" "${tunnelport}" 1
            PRINT "Agent random port: ${tunnelport}"
        fi
        PRINT "Agent ${groupid}.${agentid}.${subid} connect."
        if [ "${timeout}" -gt 0 ]; then
            PRINT "Time left: ${timeout}."
        fi
        STRING_SUBST "RUN" "{TIMEOUT}" "${timeout}" 1
        # We want to put it in background and wait for it since
        # we are trapping signals.
        _RUN_ &
        wait $!
        sleep 1  # To give parent some time to kill us.
        if [ "${timeout}" -gt 0 ]; then
            timeout=$((timeout - ($(date +%s) - startts) ))
            if [ "${timeout}" -le 0 ]; then
                PRINT "Agent ${groupid}.${agentid}.${subid} timeouted."
                break
            fi
        fi
        if [ "${reconnect}" != "1" ]; then
            PRINT "Agent ${groupid}.${agentid}.${subid} disconnected. Exiting."
            return 1
        fi
        PRINT "Agent ${groupid}.${agentid}.${subid} disconnected. Retrying in 3 sec..."
        startts=$(date +%s)
        sleep 2
    done
}

#================
#
# _RPI_AGENT_OPEN
# 
# The inner function run on the remote server which
# will repeatedly update the agent file to let others
# know that we are alive.
#
#================
_RPI_AGENT_OPEN()
{
    SPACE_SIGNATURE="groupid agentid subid bindaddress localaddress reconnect timeout title:0 healthcheck"

    local groupid="${1}"
    shift

    local agentid="${1}"
    shift

    local subid="${1}"
    shift

    local bindaddress="${1}"
    shift

    local localaddress="${1}"
    shift

    local reconnect="${1}"
    shift

    local timeout="${1}"
    shift

    local title="${1}"
    shift

    local healthcheck="${1}"
    shift

    mkdir -p "spaceagent"
    local file="spaceagent/${groupid}.${agentid}.${subid}.txt"
    local startts=
    startts=$(date +%s)
    while true; do
        printf "GroupID: %s\nAgentID: %s\nSubID: %s\nBindAddress: %s\nLocalAddress: %s\nReconnect: %s\nDate: %s\nTitle: %s\n" "${groupid}" "${agentid}" "${subid}" "${bindaddress}" "${localaddress}" "${reconnect}" "$(date +%s)" "${title}" > "${file}"
        sleep 30
        if [ "${timeout}" -gt 0 ] && [ $(($(date +%s) - startts)) -gt "${timeout}" ]; then
            break
        fi
        # We ping back as a health check on the connection.
        # If the connection has failed the ping will trigger an exit for us
        # to not have a zombie process.
        if [ "${healthcheck}" = "1" ]; then
            printf "%s\n" "Ping"
        fi
    done
}


# Disable warning about unused "SPACE_" variables
# shellcheck disable=2034

#===============
#
# RPI_AGENT_CONNECT
#
# Open a forward tunnel to remote server from this computer.
#
# Optionally state another user than the connecting user
# to access UNIX sockets for.
# TCP ports are global and are shared among all users, but UNIX sockets do belong
# to specific users.
#
#===============
RPI_AGENT_CONNECT()
{
    SPACE_SIGNATURE="localaddress remoteaddress"
    SPACE_FN="_RPI_NOOP"
    SPACE_WRAP="SSH_WRAP"
    SPACE_ARGS=""
    SPACE_OUTER="_RPI_AGENT_CONNECT_OUTER"
    SPACE_OUTERARGS="'${1}' '${2}'"
    SPACE_BUILDENV="SSHFLAGS"

    SSHFLAGS="${SSHFLAGS:+$SSHFLAGS;}-NnL{CONNECTTUNNEL}"
    YIELD "SSHFLAGS"
}

_RPI_NOOP()
{
    :
}

_RPI_AGENT_CONNECT_OUTER()
{
    # shellcheck disable=2034
    SPACE_SIGNATURE="localaddress remoteaddress"
    # shellcheck disable=2034
    SPACE_DEP="PRINT STRING_SUBST"

    local localaddress="${1}"
    shift

    local remoteaddress="${1}"
    shift

    if [ "${remoteaddress#0.0.0.0}" != "${remoteaddress}" ]; then
        remoteaddress="127.0.0.1${remoteaddress#0.0.0.0}"
    fi

    local sshtunnel="${localaddress}:$remoteaddress"

    PRINT "Setup forward tunnel: ${sshtunnel}."

    STRING_SUBST "RUN" "{CONNECTTUNNEL}" "${sshtunnel}" "1"

    _RUN_
}
