# Space Agent | [![build status](https://gitlab.com/space-sh/agent/badges/master/build.svg)](https://gitlab.com/space-sh/agent/commits/master)

An Agent is run from the Pi from where it opens
a reverse tunnel from the rendezvous server back to the Pi.

This reverse tunnel makes it so that clients can connect to the
Pi via the rendezvous server, the traffic will go via this third party.

For the Agent to be able to open a reverse tunnel
from the remote server and back it will need a ssh key
which pub key is already uploaded already to the server.
If the Agents private key is not in standard
location (~/.ssh) then you can refer to it using
the -e SSHKEYFILE flag when running /open/.
Note that when running the Agent as a system service we can
bundle the key with the Agent script to only have one file to copy to the Pi.

To generate a new key pair do:
space -m ssh /keygen/ -- mynewkey
Do not use a passphrase if the Agent should be run
in background. Upload the public key to the server
and place it in ~/.ssh/authorized_keys for the user whom the Agent will ssh as.
You can use Space for this too, to upload the pub key to the server, do:
space -m ssh /addsshkey/ -e SSHHOST=server -e SSHUSER=me -e targetuser=agentuser -e sshpubkeyfile=mynewkey.pub -s sudo
You might need sudo privileges to do this for another user than yourself, otherwise remove -s sudo

To install the Agent onto the Pi:
First dump the Agent as a standalone script:
space -m gitlab.com/rpi-shell/agent /open/ -e SSHHOST=server \
-e SSHFLAGS=-ostricthostkeychecking=no \
-e agentid=Green -e bindaddress=0.0.0.0:9333 -e localaddress=127.0.0.1:22 \
-e SSHUSER=spaceagent -e SSHKEYFILE=mynewkey -d > spaceagent.sh
chmod +x spaceagent.sh

The ssh key used could either be bundled in the script or it must
also be copied onto the card along with the agent script.
Make sure thay the variable SSHKEYFILE points correctly to the key file.

Bundle key file into script:
Dump the spaceagent.sh script with -e SSHKEYFILE='$0' to have it
refer to the script file it self, then bundle key with script as:
echo "exit" >> spaceagent.sh
cat keyfile >> spaceagent.sh
chmod 700 spaceagent.sh  # Must be restrictive since it contains a key file.

Then copy the script onto the Pi's sdcard:
# List block devices and figure out which one is the sdcard:
space -m gitlab.com/rpi-shell/manage /sdcard/list_devices/

space -m gitlab.com/rpi-shell/manage /sdcard/mount/ \
-e device=/dev/mmcblk0 -s sudo

space -m gitlab.com/rpi-shell/manage /sdcard/root/cpbin/ \
-e file=spaceagent.sh
-s sudo

If not bundled into the script then copy the ssh key file to
where SSHKEYFILE is pointing to, make sure the keyfile has 600 as permissions.

Create and enable the script as a systemd service:
space -m systemd /service/create/ -e description="The Green SpaceAgent" \
-e root=/mnt/sdcard/root
-e service=SpaceAgentGreen \
-e execstart=/bin/spaceagent.sh -s sudo

space -m systemd /service/enable/ -e root=/mnt/sdcard/root -e service=SpaceAgentGreen -s sudo

Now when the Pi is booted the Agent should be running in background.
You can see its status on the Pi (when it is running) using systemd:
systemctl status SpaceAgentGreen

Often you would want the Agent to run say five minutes every hour, to give
you a window to connect but to not drain it's battery by being constantly available.
Add -e timeout=300 when dumping the Agent script and create a systemd timer which starts the
systemd service. So create but do not enable the Agent service above, instead:
space -m systemd /timer/create/ -e description="The Green SpaceAgent timer" \
-e root=/mnt/sdcard/root
-e timer=SpaceAgentGreen \
-e oncalendar=hourly -s sudo
space -m systemd /timer/enable/ -e timer=SpaceAgentGreen \
-e root=/mnt/sdcard/root
-s sudo

Unmount sd card
space -m gitlab.com/rpi-shell/manage /sdcard/umount/ \
-e device=/dev/mmcblk0 -s sudo



## /connect/
	Open client forward tunnel to remote server

	This will create a forward tunnel from local computer to the remote server.
	The server will tunnel the traffic further to an Agent.
	 
	If the Agent who created a reverse tunnel from the server to the Pi did bind
	to a local interface, then that port is not accessible from the outside, thus
	we need a forward tunnel into the server, and our client will connect to the
	local port instead of directly to the server.
	 
	However if the Agent did bind to a public interface (0.0.0.0) then a forward
	tunnel is probably not necessary.
	 
	To get the remote address of the Agent use the /get/ node
	and filter out "BindAddress".
	 
	


## /get/
	Get specific agent information for a user

	After listing all active agents or a user,
	we could get information for a specific agent
	to whom we want to connect to.
	The information retrieved will tell us what port or socket
	the agent is listening on.
	


## /list/
	List all connected agents for a user

	Connected agents are one a per user basis,
	meaning that an Agent connects as a remote user on the system
	and all agents connecting with the same user will be grouped together.
	 
	We list agents by the user we connect as
	 
	Only active agents are listed, which are those who have
	a fresh timestamp and therefore an open tunnel.
	A timestamp not older than 30 seconds is consider a live agent.
	


## /open/
	Agent open tunnel to rendezvous server

	An agent will run this to create a tunnel
	to the rendezvous server where it will register
	it self with it's ID and the TCP port it is
	connected through.
	A user could then list all connected and live agents,
	and get information about a particular agent that is connected.
	


# Functions 

## RPI\_AGENT\_GET()  
  
  
  
  
Get the details about a specific agent.  
  
Optionally state another user than the connecting user  
to get agent for.  
  
  
  
## RPI\_AGENT\_LIST()  
  
  
  
  
List all active agents.  
  
Optionally state another user than the connecting user  
to list agents for.  
  
  
  
  
## RPI\_AGENT\_OPEN()  
  
  
  
  
Open a reverse tunnel from remote server to this device.  
  
Optionally define a TCP port or have one provided randomly.  
Bind address could be a UNIX socket, if the remote server allows that.  
  
  
  
## \_RPI\_AGENT\_OPEN\_OUTER()  
  
  
  
  
This outer function is used to randomly try ports  
until one found that is free on the server and it will  
also reconnect with the server if disconnected.  
  
  
  
## \_RPI\_AGENT\_OPEN()  
  
  
  
  
The inner function run on the remote server which  
will repeatedly update the agent file to let others  
know that we are alive.  
  
  
  
## RPI\_AGENT\_CONNECT()  
  
  
  
  
Open a forward tunnel to remote server from this computer.  
  
Optionally state another user than the connecting user  
to access UNIX sockets for.  
TCP ports are global and are shared among all users, but UNIX sockets do belong  
to specific users.  
  
  
  
